import time
import string
import random
from multiprocessing import Process, Queue


processable_data_bucket = Queue()


def random_string_generator():
    return ''.join(random.choices(string.ascii_lowercase, k=20))


def data_producer(number_of_records_to_produce):
    for index in range(number_of_records_to_produce):
        gen_string = random_string_generator()
        print(f"[PRODUCER] {index}. Generated {gen_string} and put in processable_data_bucket")
        processable_data_bucket.put(gen_string)


def data_consumer(processable_data_bucket):
    while not processable_data_bucket.empty():
        curr_string = processable_data_bucket.get()
        num_of_found_vowels = 0
        VOWELS = "aeiou"
        for alpha in curr_string:
            if alpha in VOWELS:
                num_of_found_vowels = num_of_found_vowels + 1

        print(f"{num_of_found_vowels} Vowels found in {curr_string}")

# data_producer(10)
#
# while not processable_data_bucket.empty():
#     print(processable_data_bucket.get())

if __name__ == "__main__":
    consumer_thread_1 = Process(target=data_consumer, args=(processable_data_bucket,))
    consumer_thread_2 = Process(target=data_consumer, args=(processable_data_bucket,))
    consumer_thread_3 = Process(target=data_consumer, args=(processable_data_bucket,))
    consumer_thread_4 = Process(target=data_consumer, args=(processable_data_bucket,))

    consumer_thread_1.start()
    consumer_thread_2.start()
    consumer_thread_3.start()
    consumer_thread_4.start()

    data_producer(10)

    consumer_thread_1.join()
    consumer_thread_2.join()
    consumer_thread_3.join()
    consumer_thread_4.join()
