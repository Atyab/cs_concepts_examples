import time
from multiprocessing import Process


def numeric_counter():
    for num in range(10):
        print(f"numeric_counter: {num}")
        time.sleep(0.5)


def alpha_counter():
    for character in "wajahat is here":
        print(f"alpha_counter: {character}")
        time.sleep(0.5)

if __name__ == "__main__":
    nc_thread = Process(target=numeric_counter)
    ac_thread = Process(target=alpha_counter)

    nc_thread.start()
    ac_thread.start()

    nc_thread.join()
    ac_thread.join()
